# 👁 Beholder

[![build status](https://travis-ci.com/remunizz/owldex.svg?branch=master)](https://travis-ci.com/remunizz/owldex) [![Version](https://img.shields.io/npm/v/beholderjs.svg)](https://www.npmjs.org/package/beholderjs) [![npm license](https://img.shields.io/npm/l/beholderjs.svg)](https://www.npmjs.org/package/beholderjs)  

Eyes on your app, transmute page events into structured data.

Basic event tracker library, store custom events and pageviews into localStorage.