FROM node:10.15.3-alpine
WORKDIR /var/www/owldex

COPY . ./
RUN npm install --unsafe-perm
RUN npm run build
RUN npm test
